To reproduce results: 

To use the application, follow the steps below: 

On the Play Store, download the Intel OpenCV application on the device itself.
On your computer, cd into any directory.
git clone https://gitlab.com/mnallamalli97/drowsydriver.
open Android Development Studio.
open the folder containing drowsydriver and open. 
Connect a phone or run the application on the emulator.
There may be some version issues to run the application on your device.
(my device was older so was running on Android SDK 11).
