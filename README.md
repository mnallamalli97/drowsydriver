To reproduce results: 

To use the application, follow the steps below: 

1 On the Play Store, download the Intel OpenCV application on the device itself.
2 On your computer, cd to location to clone.
3 git clone https://mnallamalli97@bitbucket.org/mnallamalli97/drowsydriver.git
4 open Android Development Studio.
5 open the folder containing drowsydriver and open. 
6 Connect a phone or run the application on the emulator.

There may be some version issues to run the application on your device.
(my device was older so was running on Android SDK 11).